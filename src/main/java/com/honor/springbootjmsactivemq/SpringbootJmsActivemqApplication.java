package com.honor.springbootjmsactivemq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootJmsActivemqApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootJmsActivemqApplication.class, args);
	}
}
